%define         debug_package %{nil}

Name:           PilotGo-plugin-a-tune
Version:        0.1.0
Release:        1
Summary:        The a-tune plugin for PilotGo supports to optimize the operating system and application using a-tune based on AI.
License:        MulanPSL-2.0
URL:            https://gitee.com/openeuler/PilotGo-plugin-a-tune
Source0:        https://gitee.com/openeuler/PilotGo-plugin-a-tune/repository/archive/v%{version}.tar.gz
# tar -xvf Source0
# cd %{name}-v%{version}/web
# run 'yarn install and yarn build' in it
# tar -czvf %{name}-web.tar.gz ../web/dist/
Source1:        PilotGo-plugin-a-tune-web.tar.gz

BuildRequires:  systemd
BuildRequires:  golang
Provides:       pilotgo-plugin-a-tune = %{version}-%{release}

%description
The a-tune plugin for PilotGo supports to optimize the operating system and application using a-tune based on AI.

%prep
%setup -n %{name}-v%{version}
tar -xzvf %{SOURCE1}

%build
pushd server
GO111MODULE=on go build -mod=vendor -o PilotGo-plugin-a-tune main.go
popd

%install
mkdir -p %{buildroot}/opt/PilotGo/plugin/a-tune/{server/{template/tar,log},web/dist}
pushd server
touch plugin_atune.log
install -D -m 0755 PilotGo-plugin-a-tune %{buildroot}/opt/PilotGo/plugin/a-tune/server
install -D -m 0644 config.yml.templete %{buildroot}/opt/PilotGo/plugin/a-tune/server/config.yml
install -D -m 0644 scripts/PilotGo-plugin-a-tune.service %{buildroot}%{_unitdir}/PilotGo-plugin-a-tune.service
install -D -m 0644 plugin_atune.log %{buildroot}/opt/PilotGo/plugin/a-tune/server/log
cp -rf template/tar %{buildroot}/opt/PilotGo/plugin/a-tune/server/template
popd
cp -rf web/dist %{buildroot}/opt/PilotGo/plugin/a-tune/web

%post
%systemd_post PilotGo-plugin-a-tune.service

%preun
%systemd_preun PilotGo-plugin-a-tune.service

%postun
%systemd_postun PilotGo-plugin-a-tune.service

%files
%dir /opt/PilotGo/plugin
%dir /opt/PilotGo/plugin/a-tune
%dir /opt/PilotGo/plugin/a-tune/server
/opt/PilotGo/plugin/a-tune/server/config.yml
/opt/PilotGo/plugin/a-tune/server/log
/opt/PilotGo/plugin/a-tune/server/template
/opt/PilotGo/plugin/a-tune/server/PilotGo-plugin-a-tune
/opt/PilotGo/plugin/a-tune/web
%{_unitdir}/PilotGo-plugin-a-tune.service

%changelog
* Mon Apr 01 2024 jianxinyu <jiangxinyu@kylinos.cn> - 0.1.0-1
- Package init
